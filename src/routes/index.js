/**
 * @author [Victor Diaz]
 * @email [victor.hamil.diaz@gmail.com]
 * @create date 2020-08-23 00:32:52
 * @modify date 2020-08-23 00:32:52
 * @desc [description]
 */

import { Router } from 'express';
import product from '../controller/product';
import account from '../controller/account';
import order from '../controller/order';
import passport from 'passport';

import initializeDb from '../db';
import {generateAccessToken, respond, authenticate} from '../middleware/auth';

let routes = Router();

initializeDb(db => {
    // ----------------- PRODUCT ENDPOINTS -----------------
    routes.get('/get-products', product.getProducts);
    routes.get('/get-product/:id', product.getProduct);
    // ----------------- ORDER ENDPOINTS -----------------
    routes.get('/get-orders', authenticate, order.getOrders);
    routes.get('/order/:id', authenticate, order.getOrderByID)
    routes.post('/order', authenticate, order.store);
    routes.delete('/order/:id', authenticate, order.deleteOrder);
    // ----------------- AUTH ENDPOINTS -----------------
    routes.post('/auth/register', account.register);
    routes.post('/auth/login', passport.authenticate('local', {session: false,scope: []}), generateAccessToken, respond);
    routes.get('/auth/logout', authenticate, account.logout);
    routes.get('/auth/me', authenticate, account.me);

    routes.get('*', product.notFound);  
});
    
export default routes;