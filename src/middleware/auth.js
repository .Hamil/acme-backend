/**
 * @author [Victor Diaz]
 * @email [victor.hamil.diaz@gmail.com]
 * @create date 2020-08-23 14:49:34
 * @modify date 2020-08-23 14:49:34
 * @desc [description]
 */


import jwt from 'jsonwebtoken';
import expressJwt from 'express-jwt';

const TOKENTIME = 60*60*24*30;
const SECRET = "shhhhhhared-secret";

let authenticate = expressJwt({ secret: SECRET });

let generateAccessToken = (req, res, next) => {
    req.token = req.token || {};
    req.token = jwt.sign({
        id: req.user.id,
    }, SECRET, {
        expiresIn: TOKENTIME //30 days
    });
    next();
}

const isAuth = (req, res, next) => {
    const token = req.headers.authorization;
  
    if (token) {
      const onlyToken = token.slice(7, token.length);
      jwt.verify(onlyToken, SECRET, (err, decode) => {
        if (err) {
          return res.status(401).send({ message: 'Invalid Token' });
        }
        req.user = decode;
        next();
        return;
      });
    } else {
      return res.status(401).send({ message: 'Token is not supplied.' });
    }
  };

let respond = (req, res) => {
    res.status(200).json({
        email: req.user.username,
        name: req.user.user,
        token: req.token
    });
}

module.exports = {
    isAuth,
    authenticate,
    generateAccessToken,
    respond
}