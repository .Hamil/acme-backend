/**
 * @author [Victor Diaz]
 * @email [victor.hamil.diaz@gmail.com]
 * @create date 2020-08-23 14:48:41
 * @modify date 2020-08-23 14:48:41
 * @desc [This is our config file]
 */


export default {
    "port": 3002,
    "bodyLimit": "100kb",
    "mongoUrl": "mongodb://localhost:27017/acme",
    "errorGettingProducts": "Something went wrong on the server :(",
    "notFound": "Endpoint not found",
    "notFoundProduct": "Product not found",
    "notFoundOrder": "Orden no encontrada",
    "newOrder": "Nueva Orden Creada"
}