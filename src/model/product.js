/**
 * @author [Victor Diaz]
 * @email [victor.hamil.diaz@gmail.com]
 * @create date 2020-08-23 13:52:36
 * @modify date 2020-08-23 13:52:36
 * @desc [Model for the products]
 */


import mongoose from 'mongoose';

let colorsSchema = new mongoose.Schema({
    color: { type: String, default: null }, 
    image: { type: String, default: null },
    price: { type: Number, default : null }
})

let sizesSchema = new mongoose.Schema({
    size: { type: String, default: null }, 
    price: { type: Number, default : null }
})

let stylesSchema = new mongoose.Schema({
    style: { type: String, default: null }, 
    image: { type: String, default: null },
    price: { type: Number, default : null }
})

let variationsSchema = new mongoose.Schema({
  colors: [ colorsSchema ],
  styles: [ stylesSchema ],
  sizes: [ sizesSchema ]
})

let schema = new mongoose.Schema({ //here we describe our model structure (putting the index true becaause when we do seeding each most be a unique collection, sometimes doesn'st work)
    name: { type: String, index: true },
    category: { type: String, index: true },
    variation: { type: String, index: true },
    variations: variationsSchema,
    image: { type: String, index: true},
    price: { type: Number, index: true},
    rating: { type: String, index: true },
    numReviews: { type: String, index: true },
    description: { type: String, index: true },
    stock: { type: Number, index: true }   
  });

let Product = mongoose.model('Product', schema);

export default Product; //we export as default because esm module require this approach