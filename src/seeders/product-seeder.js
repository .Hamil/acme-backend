import Product from '../model/product';
import mongoose from 'mongoose';
import config from '../config/index';

mongoose.connect(config.mongoUrl, {useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true});

let products = [
    new Product({
        name: 'T Shirt',
        category: 'Shirts',
        variation: 'size',
        variations: {
          colors: [
            {color: 'Gris', image: '/images/shirts/gray.jpg'}, 
            {color: 'Negro', image: '/images/shirts/black.jpg'}, 
            {color: 'Azul', image: '/images/shirts/blue.jpg'},
            {color: 'Blanco', image: '/images/shirts/white.jpg'}
          ],
          sizes: [
            {size: 'S', price: 50}, 
            {size: 'M', price: 60}, 
            {size: 'L', price: 70}
          ]          
        },
        image:'/images/shirts/gray.jpg',
        price: 50,
        rating: 4.5,
        numReviews: 10,
        description: "Este producto es un t-shirt con variaciones",
        stock: 5
    }),
    new Product({
      name: 'Taza',
      category: 'Tazas',
      variation: 'style',
      variations: {
        styles: [
          {style: 'Mug coffee cup', image: '/images/cups/mug.jpg', price: 50}, 
          {style: 'Tea cup', image: '/images/cups/tea.jpg', price: 60}, 
          {style: 'Aluminum cup', image: '/images/cups/aluminum.jpg', price: 70}
        ]
      },
      image:'/images/cups/mug.jpg',
      price: 50,
      rating: 4.5,
      numReviews: 10,
      description: "Este producto es una taza con variaciones",
      stock: 5
    }),
    new Product({
      name: 'Servicio de Promotores',
      category: 'Services',
      variation: 'size',
      variations: {},
      image:'/images/promotion/promo.jpg',
      price: 150,
      rating: 4.5,
      numReviews: 10,
      description: "Este es un servicio para promociones en ferias",
      stock: 5
    }),
    new Product({
      name: 'Servicio de Montaje',
      category: 'Services',
      variation: 'size',
      variations: {},
      image:'/images/services/mount.jpg',
      price: 250,
      rating: 4.5,
      numReviews: 10,
      description: "Este es un servicio de montaje para espacio promocional",
      stock: 5
  }),
];

let done = 0;
for (let i = 0; i < products.length; i++){
    done++;
    products[i].save((err, result) => {
        if(done === products.length){
            exit();
        }
    })
    
}

const exit = () => {
    console.log(`Products Collection are seeded :)`);
    mongoose.disconnect();
}