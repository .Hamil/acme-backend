/**
 * @author [Victor Diaz]
 * @email [victor.hamil.diaz@gmail.com]
 * @create date 2020-08-23 14:48:51
 * @modify date 2020-08-23 14:48:51
 * @desc [description]
 */


import http, { createServer } from 'http';
import express from 'express';
import bodyParser from 'body-parser';

import config from './config';
import routes from './routes';
import passport from 'passport';
const LocalStrategy = require('passport-local').Strategy;

let app = express();

app.server = http.createServer(app);

//configuring the body parser
app.use(bodyParser.json({
    limit: config.bodyLimit
}));

//passport config
app.use(passport.initialize());
let Account = require('./model/account');
passport.use(new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password'
},
    Account.authenticate()
));

passport.serializeUser(Account.serializeUser());
passport.deserializeUser(Account.deserializeUser());

// API routes
app.use('/acme', routes);

// validating 401 middleware
app.use(function (err, req, res, next) {
    if (err.name === 'UnauthorizedError') {
        res.status(401).json({code: 401, message: 'invalid token...'});
    }
    next();
});

// launching the server
app.server.listen(config.port);
console.log(`Started on port ${config.port}`);

export default app;
