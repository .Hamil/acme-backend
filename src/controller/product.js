/**
 * @author [Victor Diaz]
 * @email [victor.hamil.diaz@gmail.com]
 * @create date 2020-08-22 23:55:39
 * @modify date 2020-08-22 23:55:39
 * @desc [In this file we put all the logic for the products feature of acme]
 */


import config from '../config';
import Product from '../model/product'

/**
 * @description returns the information of a specific product
 * @param {*} req 
 * @param {*} res 
 */
const getProduct = async (req, res) => {
    try {
        let id = req.params.id;
        let product = await Product.findById(id);
        if(product)
            res.status(200).json({code: 200, message: product});
        else   
            res.status(404).json({code: 404, message: config.notFoundProduct})
        
    } catch (error) {
        res.status(500).json({code: 500, message: config.errorGettingProducts, exception: error.message});
    }
}
/**
 * @description returns all the products
 * @param {*} req 
 * @param {*} res 
 */
const getProducts = async (req, res) => {
    try {
        let products = await Product.find({});
        res.status(200).json({code: 200, message: products});
    } catch (error) {
        res.status(500).json({code: 500, message: config.errorGettingProducts, exception: error.message});
    }
}
/**
 * @description this show the client that a Endpoint does not exist
 * @param {*} req 
 * @param {*} res 
 */
const notFound = (req, res) => {
    res.status(404).json({code: 404, message: config.notFound});
}


export default({
    getProduct,
    getProducts,
    notFound
});