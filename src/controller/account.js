/**
 * @author [Victor Diaz]
 * @email [victor.hamil.diaz@gmail.com]
 * @create date 2020-08-23 00:26:23
 * @modify date 2020-08-23 00:26:23
 * @desc [description]
 */

import Account from '../model/account';
import passport from 'passport';

/**
 * @description register an account for JWT Authentication
 * @param {*} req 
 * @param {*} res 
 */

const register = (req, res) => {
    Account.register(new Account({ username: req.body.email, user: req.body.name}), req.body.password, (err, account) => { //here we put username equals to email because we have to use the email as a primary key
        if (err) {
            res.status(500).json({code: 500, message: err});
        }

        passport.authenticate(
            'local', {
                session: false
            })(req, res, () => {
                res.status(201).send({code: 201, message: "Succesfully created new account", name: req.user.user, email: req.user.username}); //sending the information that we need on the fronted (email , name)
            });
    });
}

/**
 * @description returns a JSON Web Token if the account that the client sent exist
 * @param {*} req 
 * @param {*} res 
 */

const login = (req, res) => {
    passport.authenticate(
        'local', {
            session: false,
            scope: []
        })
}

/**
 * @description destroying the token
 * @param {*} req 
 * @param {*} res 
 */

const logout = (req, res) => {
    res.logout();
    res.status(200).send({code: 200, message: 'Successfully logged out'});
}

/**
 * @description returns to the client the current user from a JWT
 * @param {*} req 
 * @param {*} res 
 */
const me = (req, res) => {
    res.status(200).json(req.user);
}

export default ({
    register,
    login,
    logout,
    me
})