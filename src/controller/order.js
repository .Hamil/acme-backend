/**
 * @author [Victor Diaz]
 * @email [victor.hamil.diaz@gmail.com]
 * @create date 2020-08-25 22:31:47
 * @modify date 2020-08-25 22:31:47
 * @desc [This is the order controller]
 */

 import Order from '../model/order'
import config from '../config';

 /**
 * @description this save a order when the user proceed the checkout
 * @param {*} req 
 * @param {*} res 
 */
const store = async (req, res) => {
    const newOrder = new Order({
        orderItems: req.body.orderItems,
        user: req.user.id,
        itemsPrice: req.body.itemsPrice,
        taxPrice: req.body.taxPrice,
        shippingPrice: req.body.shippingPrice,
        totalPrice: req.body.totalPrice
    });

    const newOrderCreated = await newOrder.save();
    res.status(201).send({ code: 201, message: config.newOrder, data: newOrderCreated });
}

/**
 * @description this show the client all the orders
 * @param {*} req 
 * @param {*} res 
 */
const getOrders = async (req, res) => {
    const orders = await Order.find({}).populate('user');
    res.status(200).json({code: 200, message: orders});
}

/**
 * @description this show the client an order for id
 * @param {*} req 
 * @param {*} res 
 */
const getOrderByID = async (req, res) => {
    const order = await Order.findOne({ _id: req.params.id });
    if (order) {
        res.status(200).json({code: 200, message: order});
    } else {
        res.status(404).json({code: 404, message: config.notFoundOrder});
    }
}

/**
 * @description this delete an order
 * @param {*} req 
 * @param {*} res 
 */
const deleteOrder = async (req, res) => {
    const order = await Order.findOne({ _id: req.params.id });
    if (order) {
      const deletedOrder = await order.remove();
      res.status(200).json({code: 200, message: deletedOrder});
    } else {
        res.status(404).json({code: 404, message: config.notFoundOrder});
    }
  }

export default(
    {store, getOrders, getOrderByID, deleteOrder}
);